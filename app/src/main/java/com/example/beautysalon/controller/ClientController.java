package com.example.beautysalon.controller;

import com.example.beautysalon.services.ClientService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@RequiredArgsConstructor
@Controller
public class ClientController {

    private final ClientService clientService;

    @GetMapping("/clients")
    public String getClient() {
        return "clients/index";
    }
}
