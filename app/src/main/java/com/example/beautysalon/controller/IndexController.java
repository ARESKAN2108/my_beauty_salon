package com.example.beautysalon.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class IndexController {

    @RequestMapping(value = {"", "/", "index", "index.html"})
    public String index() {
        System.out.println("test");
        return "index";
    }
}
