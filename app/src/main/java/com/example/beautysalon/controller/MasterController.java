package com.example.beautysalon.controller;

import com.example.beautysalon.services.MasterService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@RequiredArgsConstructor
@Controller
public class MasterController {

    private final MasterService masterService;

    @GetMapping("/masters")
    public String getClient() {
        return "masters/index";
    }
}
