package com.example.beautysalon.controller;

import com.example.beautysalon.services.OperationService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@RequiredArgsConstructor
@Controller
public class OperationController {

    private final OperationService operationService;

    @GetMapping("/operations")
    public String getClient() {
        return "operations/index";
    }
}
