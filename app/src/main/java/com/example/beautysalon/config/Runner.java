package com.example.beautysalon.config;

import com.example.beautysalon.entity.*;
import com.example.beautysalon.repository.*;
import com.example.beautysalon.services.ClientService;
import lombok.RequiredArgsConstructor;
import org.jasypt.encryption.StringEncryptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.*;

@RequiredArgsConstructor
@Component("mainRunner")
public class Runner implements CommandLineRunner {

    @Value("${spring.datasource.password:undefined}")
    private String dbPwd;

    @Qualifier("customEncryptor")
    @Autowired
    private StringEncryptor stringEncryptor;

    private final MasterRepository masterRepository;
    private final PermissionRepository permissionRepository;
    private final UserRepository userRepository;
    private final OperationRepository operationRepository;
    private final OrderItemRepository orderItemRepository;
    private final OrderRepository orderRepository;
    private final ClientRepository clientRepository;
    private final NailOperationRepository nailOperationRepository;
    private final BrowOperationRepository browOperationRepository;

    @Override
    public void run(String... args) throws Exception {

//        encryption();
//
//        initData();
////
//        createUser();
//
////        createMaster();
//
//        createOrder();

//        createClient();
//        createOperations();

//        Order order = orderRepository.findById(1L).orElseThrow();
//        List<OrderItem> orderItems = order.getOrderItems();

    }

    private void createOperations() {

        NailOperation nailOperation = NailOperation.builder()
                .operationName("asdad")
                .description("asdasd")
                .price(BigDecimal.TEN)
                .operationType(OperationType.NAILS)
                .build();

        NailOperation nailOperation2 = NailOperation.builder()
                .operationName("asdad")
                .description("asdasd")
                .price(BigDecimal.TEN)
                .operationType(OperationType.NAILS)
                .build();
        nailOperationRepository.saveAll(Arrays.asList(nailOperation, nailOperation2));

        BrowOperation browOperation = BrowOperation.builder()
                .operationName("3Д")
                .description("privet")
                .price(BigDecimal.TEN)
                .operationType(OperationType.BROWS)
                .build();

        browOperationRepository.save(browOperation);
    }

    private void createClient() {
        Client client = Client.builder()
                .firstName("olya")
                .lastName("smirnova")
                .build();
        clientRepository.save(client);

    }

    private void createUser() {

        Permission perm = permissionRepository.findByName("update_user")
                .orElseThrow(NoSuchElementException::new);

        User user1 = User.builder()
                .login("user1")
                .password("1234")
                .userRole(UserRole.ADMIN)
                .permissions(Collections.singleton(perm))
                .build();

        User user2 = User.builder()
                .login("user2")
                .password("3222")
                .userRole(UserRole.MANAGER)
                .permissions(Collections.singleton(perm))
                .build();

        userRepository.saveAll(Arrays.asList(user1, user2));
    }

    private void initData() {

        Permission permission1 = Permission.builder()
                .name("update_user")
                .build();
        Permission permission2 = Permission.builder()
                .name("delete_user")
                .build();
        Permission permission3 = Permission.builder()
                .name("create_order")
                .build();
        Permission permission4 = Permission.builder()
                .name("get_master")
                .build();

        permissionRepository.saveAll(Arrays.asList(permission1, permission2, permission3, permission4));
    }

    private void createOrder() {

        Discount discount = Discount.builder()
                .name("От двух операций и выше")
                .percent(12)
                .countOperation(4)
                .build();

        Client client = Client.builder()
                .firstName("olya")
                .contactDetails(ContactDetails.builder()
                        .phone("187236187")
                        .build())
                .lastName("smirnova")
                .build();
        clientRepository.save(client);

        Operation operation1 = Operation.builder()
                .operationType(OperationType.NAILS)
//                .name("Покрытие")
                .description("Покрытие ногтей")
                .price(BigDecimal.valueOf(10))
                .build();

        Operation operation2 = Operation.builder()
                .operationType(OperationType.EYELASHES)
//                .name("3D")
                .description("3к1")
                .price(BigDecimal.valueOf(10))
                .build();

//        operationRepository.saveAll(Arrays.asList(operation1, operation2));
//
//        Operation operation = operationRepository.findByName("Покрытие")
//                .orElseThrow(NoSuchElementException::new);
//        Operation operation3 = operationRepository.findByName("3D")
//                .orElseThrow(NoSuchElementException::new);

        Master olga = Master.builder()
                .masterType(MasterType.NAIL_MASTER)
                .operations(Set.of(operation1, operation2))
                .firstName("Ольга")
                .lastName("Маникюровна")
                .build();

//        Master svetlana = Master.builder()
//                .masterType(MasterType.LASH_MAKER)
//                .operations(Collections.singleton(operation))
//                .firstName("Светлана")
//                .lastName("Смирнова")
//                .build();

//        masterRepository.save(olga);
//        Client client = clientRepository.findById(1L).orElseThrow();

        Order order = Order.builder()
                .build();

        OrderItem orderItem = OrderItem.builder()
                .order(order)
                .master(olga)
                .operation(operation1)
                .build();

        OrderItem orderItem2 = OrderItem.builder()
                .order(order)
                .master(olga)
                .operation(operation2)
                .build();

        order.setOrderItems(Arrays.asList(orderItem, orderItem2));
        order.setClient(client);
        order.setDiscount(discount);
        orderRepository.save(order);
    }

    private void encryption() {
        String pwd = "myDev_pwd";
        String encrypt = stringEncryptor.encrypt(pwd);
        System.out.println(encrypt);
        String decrypt = stringEncryptor.decrypt(encrypt);
        System.out.println(decrypt);
        System.out.println(dbPwd);
    }
}
