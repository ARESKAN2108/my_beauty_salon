package com.example.beautysalon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan({"com.example.listener", "com.example.beautysalon"})
@SpringBootApplication
public class MyBeautySalonApplication {

    public static void main(String[] args) {
        SpringApplication.run(MyBeautySalonApplication.class, args);
    }

}
