package com.example.beautysalon.api;

import com.example.beautysalon.dto.OperationDto;
import com.example.beautysalon.services.OperationService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RequiredArgsConstructor
@RequestMapping("/api/operations")
@RestController
public class OperationRestController {

    private final OperationService operationService;

    @GetMapping
    public Collection<OperationDto> getAll(){
        return operationService.findAll();
    }
}
