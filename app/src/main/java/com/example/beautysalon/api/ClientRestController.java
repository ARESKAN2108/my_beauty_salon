package com.example.beautysalon.api;

import com.example.beautysalon.dto.ClientDto;
import com.example.beautysalon.entity.Client;
import com.example.beautysalon.entity.ClientOrder;
import com.example.beautysalon.repository.ClientOrderRepository;
import com.example.beautysalon.services.ClientService;
import com.example.beautysalon.specification.filter.ClientFilter;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.List;

@RequiredArgsConstructor
@RequestMapping("api/clients")
@RestController
public class ClientRestController {

    private final ClientService clientService;
    private final ClientOrderRepository clientOrderRepository;

    @GetMapping
    public Collection<ClientDto> getAll() {
        return clientService.findAll();
    }

    @ApiOperation(value = "Get all ClientOrder", response = ClientOrder.class, responseContainer = "List")
    @GetMapping("/view")
    public List<ClientOrder> getView() {
        return clientOrderRepository.findByPayerNameContainsIgnoreCase("a");
    }

    @ApiOperation(value = "Search clients", response = Client.class, responseContainer = "Page")
    @GetMapping("/search")
    public Page<ClientDto> searchClients(@RequestParam(required = false) String term,
                                         @RequestParam(required = false) Integer pageNumber){
        ClientFilter clientFilter = ClientFilter.builder()
                .pageNumber(pageNumber)
                .term(term)
                .build();
        return clientService.search(clientFilter);
    }

    @ApiOperation(value = "Find all clients", response = Client.class, responseContainer = "List")
    @PutMapping("/find")
    public List<ClientDto> findClients(@ApiParam(value = "Example search") @RequestBody ClientDto clientDto) {
        return clientService.findClient(clientDto);
    }
}
