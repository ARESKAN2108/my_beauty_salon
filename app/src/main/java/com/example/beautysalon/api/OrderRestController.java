package com.example.beautysalon.api;

import com.example.beautysalon.services.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/orders")
public class OrderRestController {

    private final OrderService orderService;

    @PutMapping("/{id}/cancel")
    public void cancelOrder(@PathVariable Long id) {
        orderService.cancelOrder(id);
    }

    @PutMapping("/{id}/payed")
    public void markOrderAsPayed(@PathVariable Long id) {
        orderService.markOrderAsPayed(id);
    }
}
