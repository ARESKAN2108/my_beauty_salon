package com.example.beautysalon.api;

import com.example.beautysalon.dto.SystemLogDto;
import com.example.beautysalon.dto.SystemOptionDto;
import com.example.beautysalon.services.SystemLogService;
import com.example.beautysalon.services.SystemOptionService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequiredArgsConstructor
@RequestMapping("/api/system")
@RestController
public class SystemRestController {

    private final SystemLogService systemLogService;
    private final SystemOptionService systemOptionService;

    @GetMapping("/logs")
    public List<SystemLogDto> getLogs() {
        return systemLogService.getAll();
    }

    @GetMapping("/options")
    public List<SystemOptionDto> getSystem() {
        return systemOptionService.getAll();
    }

    @GetMapping("/options/{id}")
    public SystemOptionDto getById(@PathVariable("id") String optionKey) {
        return systemOptionService.getById(optionKey);
    }

    @PutMapping("/options")
    public void saveOption(@RequestBody SystemOptionDto option) {
        systemOptionService.save(option);
    }
}
