package com.example.beautysalon.api;

import com.example.beautysalon.dto.DiscountDto;
import com.example.beautysalon.services.DiscountService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RequiredArgsConstructor
@RequestMapping("/api/discounts")
@RestController
public class DiscountRestController {

    private final DiscountService discountService;

    @GetMapping
    public Collection<DiscountDto> getAll() {
        return discountService.findAll();
    }
}
