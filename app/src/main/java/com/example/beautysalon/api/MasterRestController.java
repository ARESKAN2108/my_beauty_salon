package com.example.beautysalon.api;

import com.example.beautysalon.dto.MasterDto;
import com.example.beautysalon.entity.Master;
import com.example.beautysalon.services.MasterService;
import com.example.beautysalon.specification.filter.MasterFilter;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.List;

@RequiredArgsConstructor
@RequestMapping("/api/masters")
@RestController
public class MasterRestController {

    private final MasterService masterService;

    @GetMapping
    public Collection<MasterDto> getAll() {
        return masterService.findAll();
    }

    @ApiOperation(value = "Find all masters", response = Master.class, responseContainer = "List")
    @PutMapping("/find")
    public List<MasterDto> findMasters(@ApiParam(value = "Example search") @RequestBody MasterDto masterDto) {
        return masterService.findMaster(masterDto);
    }

    @ApiOperation(value = "Search masters", response = Master.class, responseContainer = "Page")
    @GetMapping("/search")
    public Page<MasterDto> searchMasters(@RequestParam(required = false) String term,
                                         @RequestParam(required = false) Integer pageNumber){
        MasterFilter masterFilter = MasterFilter.builder()
                .pageNumber(pageNumber)
                .term(term)
                .build();
        return masterService.search(masterFilter);
    }
}
