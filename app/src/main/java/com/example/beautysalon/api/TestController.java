package com.example.beautysalon.api;

import com.example.beautysalon.dto.ClientDto;
import com.example.beautysalon.dto.MasterDto;
import com.example.beautysalon.entity.Client;
import com.example.beautysalon.entity.ClientOrder;
import com.example.beautysalon.entity.Master;
import com.example.beautysalon.repository.ClientOrderRepository;
import com.example.beautysalon.services.ClientService;
import com.example.beautysalon.services.MasterService;
import com.example.beautysalon.specification.filter.ClientFilter;
import com.example.beautysalon.specification.filter.MasterFilter;
import com.example.system.entity.SystemOptionEntity;
import com.example.system.repository.SystemRepository;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequiredArgsConstructor
@RequestMapping("/api")
@RestController
public class TestController {

    private final SystemRepository systemRepository;

    @GetMapping("/system")
    public List<SystemOptionEntity> getSystem() {
        return systemRepository.findAll();
    }
}
