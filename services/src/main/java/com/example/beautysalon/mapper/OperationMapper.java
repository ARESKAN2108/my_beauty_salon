package com.example.beautysalon.mapper;

import com.example.beautysalon.dto.OperationDto;
import com.example.beautysalon.entity.Operation;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

@Component
@Mapper
public interface OperationMapper {

    Operation map(OperationDto dto);

    OperationDto mapToDto(Operation entity);
}
