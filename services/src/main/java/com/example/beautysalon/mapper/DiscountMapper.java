package com.example.beautysalon.mapper;

import com.example.beautysalon.dto.DiscountDto;
import com.example.beautysalon.entity.Discount;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

@Component
@Mapper
public interface DiscountMapper {

    Discount map(DiscountDto dto);

    DiscountDto mapToDto(Discount entity);
}
