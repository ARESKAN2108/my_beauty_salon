package com.example.beautysalon.mapper;

import com.example.beautysalon.dto.ContactDetailsDto;
import com.example.beautysalon.entity.ContactDetails;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

@Component
@Mapper
public interface ContactDetailsMapper {

    ContactDetails map(ContactDetailsDto dto);

    ContactDetailsDto mapToEntity(ContactDetails entity);
}
