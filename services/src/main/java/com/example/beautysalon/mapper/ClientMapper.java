package com.example.beautysalon.mapper;

import com.example.beautysalon.dto.ClientDto;
import com.example.beautysalon.entity.Client;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

@Component
@Mapper(uses = ContactDetailsMapper.class)
public interface ClientMapper {

    Client map(ClientDto dto);

    ClientDto mapToDto(Client entity);
}
