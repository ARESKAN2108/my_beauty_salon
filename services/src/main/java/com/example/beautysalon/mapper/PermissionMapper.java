package com.example.beautysalon.mapper;

import com.example.beautysalon.dto.PermissionDto;
import com.example.beautysalon.entity.Permission;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

@Component
@Mapper
public interface PermissionMapper {

    Permission map(PermissionDto dto);

    PermissionDto mapToDto(Permission entity);
}
