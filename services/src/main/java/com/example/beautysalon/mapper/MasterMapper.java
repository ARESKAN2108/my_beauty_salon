package com.example.beautysalon.mapper;

import com.example.beautysalon.dto.MasterDto;
import com.example.beautysalon.entity.Master;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

@Component
@Mapper(uses = ContactDetailsMapper.class)
public interface MasterMapper {

    Master map(MasterDto dto);

    MasterDto mapToDto(Master entity);
}
