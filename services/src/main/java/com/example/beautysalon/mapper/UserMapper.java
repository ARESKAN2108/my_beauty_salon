package com.example.beautysalon.mapper;

import com.example.beautysalon.dto.OrderDto;
import com.example.beautysalon.dto.PermissionDto;
import com.example.beautysalon.dto.UserDto;
import com.example.beautysalon.entity.Order;
import com.example.beautysalon.entity.User;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

@Component
@Mapper
public interface UserMapper {

    User map(UserDto dto);

    UserDto mapToDto(User entity);
}
