package com.example.beautysalon.mapper;

import com.example.beautysalon.dto.OrderItemDto;
import com.example.beautysalon.entity.Order;
import com.example.beautysalon.entity.OrderItem;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

@Component
@Mapper
public interface OrderItemMapper {

    OrderItem map(OrderItemDto dto);

    OrderItemDto mapToDto(OrderItem entity);
}
