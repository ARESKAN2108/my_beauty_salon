package com.example.beautysalon.mapper;

import com.example.beautysalon.dto.OrderDto;
import com.example.beautysalon.entity.Order;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

@Component
@Mapper
public interface OrderMapper {

    Order map(OrderDto dto);

    OrderDto mapToDto(Order entity);
}
