package com.example.beautysalon.mapper;

import com.example.beautysalon.dto.SystemOptionDto;
import com.example.system.entity.SystemOptionEntity;
import org.mapstruct.Mapper;

@Mapper
public interface SystemOptionMapper {

    SystemOptionDto matToDto(SystemOptionEntity entity);
}
