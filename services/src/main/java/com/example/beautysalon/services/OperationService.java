package com.example.beautysalon.services;

import com.example.beautysalon.dto.OperationDto;
import com.example.beautysalon.entity.Operation;

import java.util.Collection;
import java.util.List;

public interface OperationService extends CrudService<OperationDto, Long> {

    Collection<OperationDto> findByOperationType(String type);
}
