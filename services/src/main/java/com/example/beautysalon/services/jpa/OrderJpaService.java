package com.example.beautysalon.services.jpa;

import com.example.beautysalon.aspect.ActivityLog;
import com.example.beautysalon.dto.OrderDto;
import com.example.beautysalon.entity.Order;
import com.example.beautysalon.entity.OrderStatus;
import com.example.beautysalon.mapper.OrderMapper;
import com.example.beautysalon.repository.OrderRepository;
import com.example.beautysalon.services.OrderService;
import com.example.beautysalon.services.config.JpaImplementation;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.repository.JpaRepository;

@RequiredArgsConstructor
@JpaImplementation
public class OrderJpaService extends AbstractJpaService<OrderDto, Order, Long> implements OrderService {

    private final OrderRepository orderRepository;
    private final OrderMapper mapper;

    @Override
    public JpaRepository<Order, Long> getRepository() {
        throw new UnsupportedOperationException();
    }

    @Override
    public OrderDto mapToDto(Order entity) {
        return mapper.mapToDto(entity);
    }

    @Override
    public Order mapToEntity(OrderDto dto) {
        return mapper.map(dto);
    }

    @ActivityLog(activity = "order_cancel", value = "Order cancelled {id}")
    @Override
    public void cancelOrder(Long id) {
        OrderStatus orderStatus = orderRepository.findById(id).orElseThrow().getStatus();
        if (orderStatus != OrderStatus.PAYED) {
            orderRepository.findById(id).orElseThrow().setStatus(OrderStatus.CANCELLED);
        }
    }

    @ActivityLog(activity = "order_payed", value = "Order payed {id}")
    @Override
    public void markOrderAsPayed(Long id) {
        OrderStatus orderStatus = orderRepository.findById(id).orElseThrow().getStatus();
        if (orderStatus != OrderStatus.CANCELLED) {
            orderRepository.findById(id).orElseThrow().setStatus(OrderStatus.PAYED);
        }
    }
}
