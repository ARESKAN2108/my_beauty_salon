package com.example.beautysalon.services.map;

import com.example.beautysalon.services.BaseMapService;
import com.example.beautysalon.services.CrudService;

import java.util.Collection;

public abstract class AbstractMapService<D, ID> implements CrudService<D, ID>, BaseMapService<D, ID> {

    @Override
    public D findById(ID id) {
        return getResource().get(id);
    }

    @Override
    public void save(D entity) {
//        getResource().put(entity.getId(), entity);
    }

    @Override
    public Collection<D> findAll() {
        return getResource().values();
    }

    @Override
    public void delete(ID id) {
        getResource().remove(id);
    }
}
