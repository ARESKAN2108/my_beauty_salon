package com.example.beautysalon.services.jpa;

import com.example.beautysalon.dto.DiscountDto;
import com.example.beautysalon.entity.Discount;
import com.example.beautysalon.mapper.DiscountMapper;
import com.example.beautysalon.repository.DiscountRepository;
import com.example.beautysalon.services.DiscountService;
import com.example.beautysalon.services.config.JpaImplementation;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.repository.JpaRepository;

@RequiredArgsConstructor
@JpaImplementation
public class DiscountJpaService extends AbstractJpaService<DiscountDto, Discount, Long> implements DiscountService {

    private final DiscountRepository discountRepository;
    private final DiscountMapper mapper;

    @Override
    public JpaRepository<Discount, Long> getRepository() {
        return discountRepository;
    }

    @Override
    public DiscountDto mapToDto(Discount entity) {
        return mapper.mapToDto(entity);
    }

    @Override
    public Discount mapToEntity(DiscountDto dto) {
        return mapper.map(dto);
    }
}
