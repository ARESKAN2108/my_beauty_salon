package com.example.beautysalon.services;

import com.example.beautysalon.dto.OrderItemDto;

public interface OrderItemService extends CrudService<OrderItemDto, Long> {
}
