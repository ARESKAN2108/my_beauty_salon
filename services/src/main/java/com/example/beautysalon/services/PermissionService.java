package com.example.beautysalon.services;

import com.example.beautysalon.dto.PermissionDto;

public interface PermissionService extends CrudService<PermissionDto, Long> {
}
