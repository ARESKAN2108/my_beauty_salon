package com.example.beautysalon.services;

import com.example.beautysalon.dto.SystemOptionDto;

import java.util.List;

public interface SystemOptionService {

    List<SystemOptionDto> getAll();

    void save(SystemOptionDto option);

    SystemOptionDto getById(String optionKey);
}
