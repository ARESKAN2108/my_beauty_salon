package com.example.beautysalon.services.jpa;

import com.example.beautysalon.dto.OperationDto;
import com.example.beautysalon.entity.Operation;
import com.example.beautysalon.entity.OperationType;
import com.example.beautysalon.mapper.OperationMapper;
import com.example.beautysalon.repository.OperationRepository;
import com.example.beautysalon.services.OperationService;
import com.example.beautysalon.services.config.JpaImplementation;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Example;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@JpaImplementation
public class OperationJpaService extends AbstractJpaService<OperationDto, Operation, Long> implements OperationService {

    private final OperationRepository operationRepository;
    private final OperationMapper mapper;

    @Override
    public JpaRepository<Operation, Long> getRepository() {
        return operationRepository;
    }

    @Override
    public Collection<OperationDto> findByOperationType(String type) {
        return operationRepository.findByOperationType(OperationType.getByValue(type))
                .stream().map(mapper::mapToDto).collect(Collectors.toList());
    }

    @Override
    public OperationDto mapToDto(Operation entity) {
        return mapper.mapToDto(entity);
    }

    @Override
    public Operation mapToEntity(OperationDto dto) {
        return mapper.map(dto);
    }
}
