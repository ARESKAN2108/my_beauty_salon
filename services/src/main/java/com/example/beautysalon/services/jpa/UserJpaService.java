package com.example.beautysalon.services.jpa;

import com.example.beautysalon.dto.UserDto;
import com.example.beautysalon.entity.User;
import com.example.beautysalon.mapper.UserMapper;
import com.example.beautysalon.services.UserService;
import com.example.beautysalon.services.config.JpaImplementation;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.repository.JpaRepository;

@RequiredArgsConstructor
@JpaImplementation
public class UserJpaService extends AbstractJpaService<UserDto, User, Long> implements UserService {

    private final UserMapper mapper;

    @Override
    public JpaRepository<User, Long> getRepository() {
        throw new UnsupportedOperationException();
    }

    @Override
    public UserDto mapToDto(User entity) {
        return mapper.mapToDto(entity);
    }

    @Override
    public User mapToEntity(UserDto dto) {
        return mapper.map(dto);
    }
}
