package com.example.beautysalon.services;

import com.example.beautysalon.dto.UserDto;

public interface UserService extends CrudService<UserDto, Long> {
}
