package com.example.beautysalon.services;

import com.example.beautysalon.dto.SystemLogDto;

import java.util.List;

public interface SystemLogService {

    List<SystemLogDto> getAll();

    void createLogs(String activity, String message);
}
