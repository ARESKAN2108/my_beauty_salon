package com.example.beautysalon.services.impl;

import com.example.beautysalon.dto.SystemLogDto;
import com.example.beautysalon.mapper.SystemLogMapper;
import com.example.beautysalon.services.SystemLogService;
import com.example.listener.AuditLogService;
import com.example.system.entity.SystemLogEntity;
import com.example.system.repository.SystemLogRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@RequiredArgsConstructor
@Service
public class SystemLogServiceImpl implements SystemLogService, AuditLogService {

    @Lazy
    @Autowired
    private SystemLogRepository systemLogRepository;
    private final SystemLogMapper mapper;

    @Override
    public List<SystemLogDto> getAll() {
        return mapper.mapListToDto(systemLogRepository.findAll());
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW, transactionManager = "systemTransactionManager")
    @Override
    public void createLogs(String activity, String message) {
        systemLogRepository.save(SystemLogEntity.builder()
                .activity(activity)
                .message(message)
                .build());
    }
}
