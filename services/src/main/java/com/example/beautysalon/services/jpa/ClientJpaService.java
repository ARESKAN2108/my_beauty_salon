package com.example.beautysalon.services.jpa;

import com.example.beautysalon.dto.ClientDto;
import com.example.beautysalon.entity.Client;
import com.example.beautysalon.mapper.ClientMapper;
import com.example.beautysalon.repository.ClientRepository;
import com.example.beautysalon.services.ClientService;
import com.example.beautysalon.services.config.JpaImplementation;
import com.example.beautysalon.specification.SearchableRepository;
import com.example.beautysalon.specification.SearchableService;
import com.example.beautysalon.specification.filter.ClientFilter;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@JpaImplementation
public class ClientJpaService extends AbstractJpaService<ClientDto, Client, Long> implements ClientService, SearchableService<Client> {

    private final ClientRepository clientRepository;
    private final ClientMapper mapper;

    @Override
    public JpaRepository<Client, Long> getRepository() {
        return clientRepository;
    }

    @Override
    public SearchableRepository<Client, ?> getSearchRepository() {
        return clientRepository;
    }

    @Override
    public List<ClientDto> findClient(ClientDto clientDto) {
        ExampleMatcher caseInsensitiveExMatcher = ExampleMatcher.matchingAll().withIgnoreCase();
        Example<Client> example = Example.of(mapper.map(clientDto), caseInsensitiveExMatcher);
        return clientRepository.findAll(example).stream().map(mapper::mapToDto).collect(Collectors.toList());
    }

    @Override
    public Page<ClientDto> search(ClientFilter clientFilter) {
        return searchPage(clientFilter).map(mapper::mapToDto);
    }

    @Override
    public ClientDto mapToDto(Client entity) {
        return mapper.mapToDto(entity);
    }

    @Override
    public Client mapToEntity(ClientDto dto) {
        return mapper.map(dto);
    }
}
