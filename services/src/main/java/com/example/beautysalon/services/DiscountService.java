package com.example.beautysalon.services;

import com.example.beautysalon.dto.DiscountDto;

public interface DiscountService extends CrudService<DiscountDto, Long> {

}
