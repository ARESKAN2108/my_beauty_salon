package com.example.beautysalon.services;

import com.example.beautysalon.dto.ClientDto;
import com.example.beautysalon.entity.Client;
import com.example.beautysalon.specification.filter.ClientFilter;
import org.springframework.data.domain.Page;

import java.util.List;

public interface ClientService extends CrudService<ClientDto, Long> {

    default List<ClientDto> findClient(ClientDto clientDto) {
        throw new UnsupportedOperationException();
    }

    default Page<ClientDto> search(ClientFilter clientFilter) {
        throw new UnsupportedOperationException();
    }
}
