package com.example.beautysalon.services.jpa;

import com.example.beautysalon.dto.OrderItemDto;
import com.example.beautysalon.entity.OrderItem;
import com.example.beautysalon.mapper.OrderItemMapper;
import com.example.beautysalon.repository.OrderItemRepository;
import com.example.beautysalon.services.OrderItemService;
import com.example.beautysalon.services.config.JpaImplementation;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.repository.JpaRepository;

@RequiredArgsConstructor
@JpaImplementation
public class OrderItemJpaService extends AbstractJpaService<OrderItemDto, OrderItem, Long> implements OrderItemService {

    private final OrderItemRepository orderItemRepository;
    private final OrderItemMapper mapper;

    @Override
    public JpaRepository<OrderItem, Long> getRepository() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void delete(Long id) {

    }

    @Override
    public OrderItemDto mapToDto(OrderItem entity) {
        return mapper.mapToDto(entity);
    }

    @Override
    public OrderItem mapToEntity(OrderItemDto dto) {
        return mapper.map(dto);
    }
}
