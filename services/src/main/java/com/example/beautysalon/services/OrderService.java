package com.example.beautysalon.services;

import com.example.beautysalon.dto.OrderDto;

public interface OrderService extends CrudService<OrderDto, Long> {

    void cancelOrder(Long id);

    void markOrderAsPayed(Long id);
}
