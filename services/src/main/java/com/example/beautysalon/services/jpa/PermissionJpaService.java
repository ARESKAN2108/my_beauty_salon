package com.example.beautysalon.services.jpa;

import com.example.beautysalon.dto.PermissionDto;
import com.example.beautysalon.entity.Permission;
import com.example.beautysalon.mapper.PermissionMapper;
import com.example.beautysalon.services.PermissionService;
import com.example.beautysalon.services.config.JpaImplementation;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.repository.JpaRepository;

@RequiredArgsConstructor
@JpaImplementation
public class PermissionJpaService extends AbstractJpaService<PermissionDto, Permission, Long> implements PermissionService {

    private final PermissionMapper mapper;

    @Override
    public JpaRepository<Permission, Long> getRepository() {
        throw new UnsupportedOperationException();
    }

    @Override
    public PermissionDto mapToDto(Permission entity) {
        return mapper.mapToDto(entity);
    }

    @Override
    public Permission mapToEntity(PermissionDto dto) {
        return mapper.map(dto);
    }
}
