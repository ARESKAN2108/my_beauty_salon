package com.example.beautysalon.services.map;

import com.example.beautysalon.dto.PermissionDto;
import com.example.beautysalon.entity.Permission;
import com.example.beautysalon.services.PermissionService;
import com.example.beautysalon.services.config.MapImplementation;

import java.util.HashMap;
import java.util.Map;

@MapImplementation
public class PermissionMapService extends AbstractMapService<PermissionDto,Long> implements PermissionService {

    private static final Map<Long, PermissionDto> resource = new HashMap<>();

    @Override
    public Map<Long, PermissionDto> getResource() {
        return resource;
    }
}
