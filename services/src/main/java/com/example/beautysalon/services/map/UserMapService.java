package com.example.beautysalon.services.map;

import com.example.beautysalon.dto.UserDto;
import com.example.beautysalon.entity.User;
import com.example.beautysalon.services.UserService;
import com.example.beautysalon.services.config.MapImplementation;

import java.util.HashMap;
import java.util.Map;

@MapImplementation
public class UserMapService extends AbstractMapService<UserDto, Long> implements UserService {

    private static final Map<Long, UserDto> resource = new HashMap<>();

    @Override
    public Map<Long, UserDto> getResource() {
        return resource;
    }
}
