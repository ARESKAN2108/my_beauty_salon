package com.example.beautysalon.services.impl;

import com.example.beautysalon.dto.SystemOptionDto;
import com.example.beautysalon.mapper.SystemOptionMapper;
import com.example.beautysalon.services.SystemLogService;
import com.example.beautysalon.services.SystemOptionService;
import com.example.system.entity.SystemOptionEntity;
import com.example.system.repository.SystemRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class SystemOptionServiceImpl implements SystemOptionService {

    private final SystemRepository systemRepository;
    private final SystemOptionMapper mapper;

    @Override
    public List<SystemOptionDto> getAll() {
        return systemRepository.findAll().stream().map(mapper::matToDto).collect(Collectors.toList());
    }

    @Override
    public SystemOptionDto getById(String optionKey) {
        return systemRepository.findById(optionKey).map(mapper::matToDto).orElseThrow();
    }

    @Transactional(transactionManager = "systemTransactionManager")
    @Override
    public void save(SystemOptionDto option) {
        SystemOptionEntity entity = systemRepository.findById(option.getId())
                .orElse(SystemOptionEntity.builder().id(option.getId()).build());

        entity.setValue(option.getValue());
        systemRepository.save(entity);
    }
}
