package com.example.beautysalon.services.map;

import com.example.beautysalon.dto.OperationDto;
import com.example.beautysalon.entity.Operation;
import com.example.beautysalon.services.OperationService;
import com.example.beautysalon.services.config.MapImplementation;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@MapImplementation
public class OperationMapService extends AbstractMapService<OperationDto, Long> implements OperationService {

    private static final Map<Long, OperationDto> resource = new HashMap<>();

    @Override
    public Map<Long, OperationDto> getResource() {
        return resource;
    }

    @Override
    public Collection<OperationDto> findByOperationType(String type) {
        return null; //сделать логику
    }
}

