package com.example.beautysalon.services;

import java.util.Map;

public interface BaseMapService<D, ID> {

    Map<ID, D> getResource();
}
