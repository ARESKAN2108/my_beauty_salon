package com.example.beautysalon.services.map;

import com.example.beautysalon.dto.MasterDto;
import com.example.beautysalon.services.MasterService;
import com.example.beautysalon.services.config.MapImplementation;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@MapImplementation
public class MasterMapService extends AbstractMapService<MasterDto, Long> implements MasterService {

    private static final Map<Long, MasterDto> resource = new HashMap<>();

    @Override
    public Map<Long, MasterDto> getResource() {
        return resource;
    }

    @Override
    public Collection<MasterDto> findByFirstName(String FirstName) {
        return null; //сделать логику
    }
}
