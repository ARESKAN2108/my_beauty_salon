package com.example.beautysalon.services;

import com.example.beautysalon.dto.MasterDto;
import com.example.beautysalon.entity.Master;
import com.example.beautysalon.specification.filter.MasterFilter;
import org.springframework.data.domain.Page;

import java.util.Collection;
import java.util.List;

public interface MasterService extends CrudService<MasterDto, Long> {

    default Collection<MasterDto> findByFirstName(String firstName) {
        throw new UnsupportedOperationException();
    }

    default List<MasterDto> findMaster(MasterDto MasterDto) {
        throw new UnsupportedOperationException();
    }

    default Page<MasterDto> search(MasterFilter masterFilter) {
        throw new UnsupportedOperationException();
    }
}
