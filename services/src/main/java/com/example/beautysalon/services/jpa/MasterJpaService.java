package com.example.beautysalon.services.jpa;

import com.example.beautysalon.dto.MasterDto;
import com.example.beautysalon.entity.Master;
import com.example.beautysalon.mapper.MasterMapper;
import com.example.beautysalon.repository.MasterRepository;
import com.example.beautysalon.services.MasterService;
import com.example.beautysalon.services.config.JpaImplementation;
import com.example.beautysalon.specification.SearchableRepository;
import com.example.beautysalon.specification.SearchableService;
import com.example.beautysalon.specification.filter.MasterFilter;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@JpaImplementation
public class MasterJpaService extends AbstractJpaService<MasterDto, Master, Long> implements MasterService, SearchableService<Master> {

    private final MasterRepository masterRepository;
    private final MasterMapper mapper;

    @Override
    public JpaRepository<Master, Long> getRepository() {
        return masterRepository;
    }

    @Override
    public SearchableRepository<Master, ?> getSearchRepository() {
        return masterRepository;
    }

    @Override
    public Collection<MasterDto> findByFirstName(String firstName) {
        return masterRepository.findByFirstNameLike(firstName).stream().map(mapper::mapToDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<MasterDto> findMaster(MasterDto masterDto) {
        ExampleMatcher caseInsensitiveExMatcher = ExampleMatcher.matchingAll().withIgnoreCase();
        Example<Master> example = Example.of(mapper.map(masterDto), caseInsensitiveExMatcher);
        return masterRepository.findAll(example).stream().map(mapper::mapToDto).collect(Collectors.toList());
    }

    @Override
    public Page<MasterDto> search(MasterFilter masterFilter) {
        return searchPage(masterFilter).map(mapper::mapToDto);
    }

    @Override
    public MasterDto mapToDto(Master entity) {
        return mapper.mapToDto(entity);
    }

    @Override
    public Master mapToEntity(MasterDto dto) {
        return mapper.map(dto);
    }
}
