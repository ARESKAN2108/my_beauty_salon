package com.example.beautysalon.dto;

import lombok.Data;

@Data
public class ContactDetailsDto {

    private String phone;
    private String city;
//    private String street;
//    private String building;
}
