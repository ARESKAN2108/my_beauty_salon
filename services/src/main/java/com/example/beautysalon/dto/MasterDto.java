package com.example.beautysalon.dto;

import com.example.beautysalon.entity.MasterType;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.time.OffsetDateTime;

@Data
public class MasterDto {

    private Long id;
    private String qualification;
    private MasterType masterType;
    @JsonProperty("details")
    private ContactDetailsDto contactDetails;
    private String firstName;
    private String lastName;
    private String fullName;

    private OffsetDateTime createdAt;
    private String createdBy;
    private OffsetDateTime lastModifiedAt;
    private String lastModifiedBy;
}
