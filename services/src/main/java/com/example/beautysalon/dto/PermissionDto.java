package com.example.beautysalon.dto;

import lombok.Data;

@Data
public class PermissionDto {

    private Long id;
    private String name;
}
