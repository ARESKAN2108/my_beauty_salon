package com.example.beautysalon.dto;

import lombok.Data;

@Data
public class DiscountDto {

    private Long id;
    private String name;
    private int countOperation;
    private int percent;
}
