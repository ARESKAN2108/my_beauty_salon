package com.example.beautysalon.dto;

import com.example.beautysalon.entity.OperationType;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class OperationDto {

    private Long id;
    private String description;
    private String operationName;
    private BigDecimal price;
    private OperationType operationType;

}
