package com.example.beautysalon.repository;

import com.example.beautysalon.entity.EyeLashOperation;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EyeLashRepository extends JpaRepository<EyeLashOperation, Long> {
}
