package com.example.beautysalon.repository;

import com.example.beautysalon.entity.OrderItem;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface OrderItemRepository extends JpaRepository<OrderItem, Long> {

    List<OrderItem> findOrderItemByCreatedBy(String createdBy);
}
