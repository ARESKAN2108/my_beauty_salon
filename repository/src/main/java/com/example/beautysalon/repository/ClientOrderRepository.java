package com.example.beautysalon.repository;

import com.example.beautysalon.entity.ClientOrder;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ClientOrderRepository extends JpaRepository<ClientOrder, Long> {

    List<ClientOrder> findByPayerNameContainsIgnoreCase(String name);
}
