package com.example.beautysalon.repository;

import com.example.beautysalon.entity.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.Optional;

public interface OrderRepository extends JpaRepository<Order, Long> {

    Optional<Order> findByAmount(BigDecimal price);

    List<Order> findByDate(OffsetDateTime date);

    List<Order> findByClientLastName(String client_lastName);

    @Transactional
    @Modifying
    @Query("DELETE FROM Order o where o.status = com.example.beautysalon.entity.OrderStatus.CANCELLED")
    void deleteCancelled();
}
