package com.example.beautysalon.repository;

import com.example.beautysalon.entity.Client;
import com.example.beautysalon.specification.SearchableRepository;

import java.util.List;

public interface ClientRepository extends PersonRepository<Client, Long>, SearchableRepository<Client, Long> {

    List<Client> findClientByContactDetailsCity(String city);

    List<Client> findClientByContactDetailsPhone(String phone);

    List<Client> findByLastNameIgnoreCase(String lastName);

    List<Client> findByLastNameContaining(String name);

}
