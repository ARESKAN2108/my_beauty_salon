package com.example.beautysalon.repository;

import com.example.beautysalon.entity.NailOperation;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NailOperationRepository extends JpaRepository<NailOperation, Long> {
}
