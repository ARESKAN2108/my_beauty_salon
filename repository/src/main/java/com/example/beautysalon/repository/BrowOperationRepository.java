package com.example.beautysalon.repository;

import com.example.beautysalon.entity.BrowOperation;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BrowOperationRepository extends JpaRepository<BrowOperation, Long> {
}
