package com.example.beautysalon.repository;

import com.example.beautysalon.entity.Discount;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DiscountRepository extends JpaRepository<Discount, Long> {

    Discount findByName(String name);

    Discount findByCountOperation(int count);

    Discount findByPercent(int percent);
}
