package com.example.beautysalon.repository;

import com.example.beautysalon.entity.Master;
import com.example.beautysalon.entity.MasterType;
import com.example.beautysalon.specification.SearchableRepository;

import java.util.List;

public interface MasterRepository extends PersonRepository<Master, Long>, SearchableRepository<Master, Long> {

    List<Master> findMasterByQualification(String qualification);

    List<Master> findByMasterType(MasterType masterType);

    List<Master> findByContactDetailsPhone(String contactDetails_phone);

    List<Master> findByLastNameIgnoreCase(String lastName);

    List<Master> findByLastNameContaining(String name);

    List<Master> findByFirstNameLike(String name);
}
