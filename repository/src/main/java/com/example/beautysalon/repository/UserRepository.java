package com.example.beautysalon.repository;

import com.example.beautysalon.entity.User;
import com.example.beautysalon.entity.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserRepository extends JpaRepository<User, Long> {

    List<User> findByUserRole(UserRole userRole);
}
