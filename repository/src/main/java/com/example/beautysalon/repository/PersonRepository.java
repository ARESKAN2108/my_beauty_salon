package com.example.beautysalon.repository;

import com.example.beautysalon.entity.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.Collection;
import java.util.List;

@NoRepositoryBean
public interface PersonRepository<T extends Person, ID> extends JpaRepository<T, ID> {

    List<T> findByFirstName(String firstName);

    List<T> findByLastName(String lastName);

    List<T> findByFullName(String fullName);
}
