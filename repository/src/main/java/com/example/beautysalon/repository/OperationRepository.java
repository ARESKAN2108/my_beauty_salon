package com.example.beautysalon.repository;

import com.example.beautysalon.entity.Operation;
import com.example.beautysalon.entity.OperationType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

public interface OperationRepository extends JpaRepository<Operation, Long> {

    Optional<Operation> findByPrice(BigDecimal price);

    List<Operation> findByOperationType(OperationType operationType);
}
