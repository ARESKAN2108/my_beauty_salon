package com.example.beautysalon.specification.filter;

import com.example.beautysalon.entity.Client;
import com.example.beautysalon.entity.Client_;
import com.example.beautysalon.specification.ClientSpecification;
import lombok.Builder;
import lombok.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

import java.util.Objects;

@Builder
@Value
public class ClientFilter implements Filter<Client> {

    private static final Sort DEFAULT_SORTING = Sort.by(Sort.Direction.DESC, Client_.CREATED_AT);

    Integer pageNumber;
    String term;

    String firstName;
    String lastName;

    @Override
    public Specification<Client> getSpecification() {
        return ClientSpecification.builder().filter(this).build();
    }

    @Override
    public Pageable getPageable() {
        int page = Objects.isNull(pageNumber) ? 0 : pageNumber - 1;
        return PageRequest.of(page, DEFAULT_PAGE_SIZE, DEFAULT_SORTING);

    }
}
