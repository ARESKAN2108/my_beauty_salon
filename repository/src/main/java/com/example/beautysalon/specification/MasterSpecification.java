package com.example.beautysalon.specification;

import com.example.beautysalon.entity.*;
import com.example.beautysalon.specification.filter.MasterFilter;
import com.example.beautysalon.specification.filter.SpecificationBuilder;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;

public interface MasterSpecification extends Specification<Master> {

    static MasterSpecification hasFirstName(String value) {
        return ((root, cQuery, cBuilder) -> cBuilder.equal(cBuilder.lower(root.get(Master_.FIRST_NAME)),
                value.toLowerCase()));
    }

    static MasterSpecification firstNameLike(String value) {
        return ((root, cQuery, cBuilder) -> cBuilder.like(cBuilder.lower(root.get(Master_.FIRST_NAME)),
                "%" + value.toLowerCase() + "%"));
    }

    static MasterSpecification hasLastName(String value) {
        return ((root, cQuery, cBuilder) -> cBuilder.equal(cBuilder.lower(root.get(Master_.LAST_NAME)),
                value.toLowerCase()));
    }

    static MasterSpecification lastNameLike(String value) {
        return ((root, cQuery, cBuilder) -> cBuilder.like(cBuilder.lower(root.get(Master_.LAST_NAME)),
                "%" + value.toLowerCase() + "%"));
    }

    static MasterSpecification qualificationLike(String value) {
        return ((root, cQuery, cBuilder) -> cBuilder.like(cBuilder.lower(root.get(Master_.QUALIFICATION)),
                "%" + value.toLowerCase() + "%"));
    }

    static MasterSpecification hasMasterType(String value) {
        return ((root, cQuery, cBuilder) -> cBuilder.equal((cBuilder.lower(root.get(Master_.MASTER_TYPE))), value));
    } // не знаю как достучаться до enum поля MasterType через specification, не происходит поиск мастеров по типу.

    static MasterSpecification cityNameLike(String value) {
        return ((root, cQuery, cBuilder) -> cBuilder.like(cBuilder.lower(root.get(Master_.CONTACT_DETAILS).get(ContactDetails_.CITY)),
                "%" + value.toLowerCase() + "%"));
    }

    static MasterSpecification phoneNameLike(String value) {
        return ((root, cQuery, cBuilder) -> cBuilder.like(cBuilder.lower(root.get(Master_.CONTACT_DETAILS).get(ContactDetails_.PHONE)),
                "%" + value.toLowerCase() + "%"));
    }

    static Specification<Master> findByTerm(String term) {
        List<Specification<Master>> specificationList = new ArrayList<>();
        specificationList.add(firstNameLike(term));
        specificationList.add(lastNameLike(term));
        specificationList.add(cityNameLike(term));
        specificationList.add(phoneNameLike(term));
        specificationList.add(qualificationLike(term));

        return SpecificationComposer.compose(specificationList, Predicate.BooleanOperator.OR);
    }

    static MasterSpecification.Builder builder() {
        return new MasterSpecification.Builder();
    }

    class Builder extends SpecificationBuilder<Master, MasterFilter> {

        @Override
        public Specification<Master> build() {
            List<Specification<Master>> specList = new ArrayList<>();

            if (StringUtils.isNotEmpty(filter.getFirstName())) {
                specList.add(hasFirstName(filter.getFirstName()));
            }
            if (StringUtils.isNotEmpty(filter.getLastName())) {
                specList.add(hasLastName(filter.getLastName()));
            }
            if (StringUtils.isNotEmpty(filter.getTerm())) {
                specList.add(findByTerm(filter.getTerm()));
            }
            if (StringUtils.isNotEmpty(filter.getMasterType().getValue())) {
                specList.add(hasMasterType(filter.getMasterType().getValue()));
            }
            return SpecificationComposer.compose(specList);
        }
    }
}
