package com.example.beautysalon.specification.filter;

import com.example.beautysalon.entity.Master;
import com.example.beautysalon.entity.MasterType;
import com.example.beautysalon.entity.Master_;
import com.example.beautysalon.specification.MasterSpecification;
import lombok.Builder;
import lombok.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

import java.util.Objects;

@Builder
@Value
public class MasterFilter implements Filter<Master> {

    private static final Sort DEFAULT_SORTING = Sort.by(Sort.Direction.DESC, Master_.CREATED_AT);

    Integer pageNumber;
    String term;

    String firstName;
    String lastName;
    String qualification;
    MasterType masterType;

    @Override
    public Specification<Master> getSpecification() {
        return MasterSpecification.builder().filter(this).build();
    }

    @Override
    public Pageable getPageable() {
        int page = Objects.isNull(pageNumber) ? 0 : pageNumber - 1;
        return PageRequest.of(page, DEFAULT_PAGE_SIZE, DEFAULT_SORTING);

    }
}
