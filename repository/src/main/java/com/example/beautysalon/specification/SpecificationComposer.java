package com.example.beautysalon.specification;

import com.example.beautysalon.entity.Client;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Predicate;
import java.util.List;

public class SpecificationComposer {

    static <T> Specification<T> compose(List<Specification<T>> specificationList) {
        return compose(specificationList, Predicate.BooleanOperator.AND);
    }

    static <T> Specification<T> compose(List<Specification<T>> specificationList, Predicate.BooleanOperator operator) {

        if (CollectionUtils.isEmpty(specificationList)) return null;

        Specification<T> result = null;
        for (Specification<T> spec : specificationList) {
            if (result == null) {
                result = Specification.where(spec);
            } else if (Predicate.BooleanOperator.OR == operator) {
                result = result.or(spec);
            } else {
                result = result.and(spec);
            }
        }
        return result;
    }
}
