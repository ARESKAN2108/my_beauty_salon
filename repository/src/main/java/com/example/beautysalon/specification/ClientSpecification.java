package com.example.beautysalon.specification;

import com.example.beautysalon.entity.Client;
import com.example.beautysalon.entity.Client_;
import com.example.beautysalon.entity.ContactDetails_;
import com.example.beautysalon.specification.filter.ClientFilter;
import com.example.beautysalon.specification.filter.SpecificationBuilder;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;

public interface ClientSpecification extends Specification<Client> {

    static ClientSpecification hasFirstName(String value) {
        return ((root, cQuery, cBuilder) -> cBuilder.equal(cBuilder.lower(root.get(Client_.FIRST_NAME)),
                value.toLowerCase()));
    }

    static ClientSpecification firstNameLike(String value) {
        return ((root, cQuery, cBuilder) -> cBuilder.like(cBuilder.lower(root.get(Client_.FIRST_NAME)),
                "%" + value.toLowerCase() + "%"));
    }

    static ClientSpecification lastNameLike(String value) {
        return ((root, cQuery, cBuilder) -> cBuilder.like(cBuilder.lower(root.get(Client_.LAST_NAME)),
                "%" + value.toLowerCase() + "%"));
    }

    static ClientSpecification hasLastName(String value) {
        return ((root, cQuery, cBuilder) -> cBuilder.equal(cBuilder.lower(root.get(Client_.LAST_NAME)),
                value.toLowerCase()));
    }

    static ClientSpecification cityNameLike(String value) {
        return ((root, cQuery, cBuilder) -> cBuilder.like(cBuilder.lower(root.get(Client_.CONTACT_DETAILS).get(ContactDetails_.CITY)),
                "%" + value.toLowerCase() + "%"));
    }

    static ClientSpecification phoneNameLike(String value) {
        return ((root, cQuery, cBuilder) -> cBuilder.like(cBuilder.lower(root.get(Client_.CONTACT_DETAILS).get(ContactDetails_.PHONE)),
                "%" + value.toLowerCase() + "%"));
    }

    static Specification<Client> findByTerm(String term){
        List<Specification<Client>> specificationList = new ArrayList<>();
        specificationList.add(firstNameLike(term));
        specificationList.add(lastNameLike(term));
        specificationList.add(cityNameLike(term));
        specificationList.add(phoneNameLike(term));

        return SpecificationComposer.compose(specificationList, Predicate.BooleanOperator.OR);
    }

    static ClientSpecification.Builder builder() {
        return new ClientSpecification.Builder();
    }

    class Builder extends SpecificationBuilder<Client, ClientFilter> {

        @Override
        public Specification<Client> build() {
            List<Specification<Client>> specList = new ArrayList<>();

            if (StringUtils.isNotEmpty(filter.getFirstName())) {
                specList.add(hasFirstName(filter.getFirstName()));
            }
            if (StringUtils.isNotEmpty(filter.getLastName())) {
                specList.add(hasLastName(filter.getLastName()));
            }
            if (StringUtils.isNotEmpty(filter.getTerm())) {
                specList.add(findByTerm(filter.getTerm()));
            }
            return SpecificationComposer.compose(specList);
        }
    }
}
