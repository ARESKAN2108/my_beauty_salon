package com.example.system.entity;

import com.example.listener.SystemOptionEntityListener;
import lombok.*;

import javax.persistence.*;

@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Setter
@Getter
@Entity
@EntityListeners(SystemOptionEntityListener.class)
@Table(name = "SYSTEM_OPTION")
public class SystemOptionEntity {

    @Id
    @Column(name = "ID")
    private String id;

    @Column(name = "VALUE")
    private String value;
}
