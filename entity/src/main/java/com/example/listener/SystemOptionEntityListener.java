package com.example.listener;

import com.example.system.entity.SystemOptionEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.PostPersist;
import javax.persistence.PostRemove;
import javax.persistence.PostUpdate;

@Component
@Slf4j
public class SystemOptionEntityListener extends BaseLogEntityListener {

    private static final String AUDIT_ACTIVITY = "system_option";

    @Autowired
    public SystemOptionEntityListener(AuditLogService auditLogService) {
        super(auditLogService);
    }

    @Override
    protected String getLogActivity() {
        return AUDIT_ACTIVITY;
    }

    @PostPersist
    public void methodInvokedAfterPersist(SystemOptionEntity entity) {
        super.methodInvokedAfterPersist(entity);
    }

    @PostUpdate
    public void methodInvokedAfterUpdate(SystemOptionEntity entity) {
        super.methodInvokedAfterUpdate(entity);
    }

    @PostRemove
    public void methodInvokedAfterDelete(SystemOptionEntity entity) {
        super.methodInvokedAfterDelete(entity);
    }
}
