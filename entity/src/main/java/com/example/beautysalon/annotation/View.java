package com.example.beautysalon.annotation;

import org.hibernate.annotations.Immutable;

@Immutable
public @interface View {
}
