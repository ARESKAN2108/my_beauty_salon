package com.example.beautysalon.converter;

import com.example.beautysalon.entity.OperationType;
import org.springframework.stereotype.Component;

import javax.persistence.AttributeConverter;

@Component
public class OperationTypeConverter implements AttributeConverter<OperationType, String> {

    @Override
    public String convertToDatabaseColumn(OperationType operationType) {
        return operationType.getValue();
    }

    @Override
    public OperationType convertToEntityAttribute(String dbData) {
        return OperationType.getByValue(dbData);
    }
}
