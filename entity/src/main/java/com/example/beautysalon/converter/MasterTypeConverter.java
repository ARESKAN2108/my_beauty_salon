package com.example.beautysalon.converter;

import com.example.beautysalon.entity.MasterType;
import org.springframework.stereotype.Component;

import javax.persistence.AttributeConverter;

@Component
public class MasterTypeConverter implements AttributeConverter<MasterType, String> {

    @Override
    public String convertToDatabaseColumn(MasterType masterType) {
        return masterType.getValue();
    }

    @Override
    public MasterType convertToEntityAttribute(String dbData) {
        return MasterType.getByValue(dbData);
    }

}
