package com.example.beautysalon.converter;

import com.example.beautysalon.entity.UserRole;
import org.springframework.stereotype.Component;

import javax.persistence.AttributeConverter;

@Component
public class UserRoleConverter implements AttributeConverter<UserRole, String> {

    @Override
    public String convertToDatabaseColumn(UserRole userRole) {
        return userRole.getValue();
    }

    @Override
    public UserRole convertToEntityAttribute(String dbData) {
        return UserRole.getByValue(dbData);
    }
}
