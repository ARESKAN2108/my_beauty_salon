package com.example.beautysalon.entity;

import com.example.beautysalon.converter.OperationTypeConverter;
import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@Setter
@Getter
@Table(name = "OPERATIONS")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "operation_type", discriminatorType = DiscriminatorType.STRING)
@Entity
public class Operation extends BaseEntity<Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "OPERATION_NAME")
    private String operationName;

    @Column(name = "PRICE")
    private BigDecimal price;

    @Column(name = "OPERATION_TYPE", nullable = false, insertable = false, updatable = false)
    @Convert(converter = OperationTypeConverter.class)
    private OperationType operationType;

    @ManyToMany(mappedBy = "operations", cascade = {CascadeType.DETACH, CascadeType.REFRESH})
    private List<Master> masters;

    @OneToMany(mappedBy = "operation", cascade = CascadeType.ALL)
    private List<OrderItem> orderItems;
}
