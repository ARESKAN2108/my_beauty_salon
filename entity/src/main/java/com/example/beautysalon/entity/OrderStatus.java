package com.example.beautysalon.entity;

public enum OrderStatus {

    PENDING, PAYED, CANCELLED
}
