package com.example.beautysalon.entity;

import com.example.beautysalon.exception.NoSuchRoleException;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Arrays;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Getter
public enum UserRole {

    ADMIN("Admin"), MANAGER("Mgr"), USER("Usr");

    private static final Map<String, UserRole> MAP = Arrays.stream(UserRole.values())
            .collect(Collectors.toMap(UserRole::getValue, Function.identity()));

    private final String value;

    public static UserRole getByValue(String value) {
        if (Objects.isNull(value)) {
            throw new NoSuchRoleException("Such a role doesn't exist");
        }
        UserRole userRole = MAP.get(value);
        return Objects.isNull(userRole) ? USER : userRole;
    }

}
