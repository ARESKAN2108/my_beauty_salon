package com.example.beautysalon.entity;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@SuperBuilder
@DiscriminatorValue(value = OperationType.Values.NAILS)
@Entity
public class NailOperation extends Operation {

    @Column(name = "NAIL_OPERATION_NAME")
    private String nailOperationName;
}
