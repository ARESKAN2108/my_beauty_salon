package com.example.beautysalon.entity;

import lombok.*;
import org.apache.commons.collections4.CollectionUtils;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.List;

@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Setter
@Getter
@Table(name = "ORDERS")
@Entity
public class Order extends BaseEntity<Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ORDER_ID")
    private Long id;

    @OneToMany(mappedBy = "order", cascade = CascadeType.ALL)
    private List<OrderItem> orderItems;

    @ManyToOne
    @JoinColumn(name = "CLIENT_ID")
    private Client client;

    @Column(name = "DATE")
    private OffsetDateTime date;

    @OneToOne(cascade = {CascadeType.PERSIST, CascadeType.DETACH}, orphanRemoval = true)
    @JoinColumn(name = "ORDER_DISCOUNT_ID")
    private Discount discount;

    @Column(name = "AMOUNT")
    private BigDecimal amount;

    @Builder.Default
    @Enumerated(EnumType.STRING)
    private OrderStatus status = OrderStatus.PENDING;

    @PrePersist
    @PreUpdate
    private void calculateAmount() {
        BigDecimal amount = BigDecimal.ZERO;
        if (CollectionUtils.isNotEmpty(orderItems)) {
            if (orderItems.size() >= discount.getCountOperation()) {
                for (OrderItem orderItem : orderItems) {
                    amount = amount.add(orderItem.getOperation().getPrice().multiply(new BigDecimal("0.9")));
                }
            } else {
                for (OrderItem orderItem : orderItems) {
                    amount = amount.add(orderItem.getOperation().getPrice());
                }
            }
        }
        this.amount = amount;
    }

}
