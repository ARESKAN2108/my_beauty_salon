package com.example.beautysalon.entity;

import lombok.*;
import org.springframework.boot.context.properties.bind.DefaultValue;

import javax.persistence.*;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Entity
public class Discount extends BaseEntity<Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "COUNT_OPERATION", nullable = false)
    @Builder.Default
    private int countOperation = 3;

    @Column(name = "PERCENT", nullable = false)
    @Builder.Default
    private int percent = 10;

    @OneToOne(mappedBy = "discount")
    private Order order;
}
