package com.example.beautysalon.entity;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Arrays;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Getter
public enum OperationType {

    NAILS(Values.NAILS),
    BROWS(Values.BROWS),
    EYELASHES(Values.EYELASHES);

    private static final Map<String, OperationType> MAP = Arrays.stream(OperationType.values())
            .collect(Collectors.toMap(OperationType::getValue, Function.identity()));

    private final String value;

    public static OperationType getByValue(String value) {
        if (Objects.isNull(value)) {
            return null;
        }
        return MAP.get(value);
    }

    public static class Values {

        public static final String NAILS = "NAILS";
        public static final String BROWS = "BROWS";
        public static final String EYELASHES = "EYELASHES";
    }
}