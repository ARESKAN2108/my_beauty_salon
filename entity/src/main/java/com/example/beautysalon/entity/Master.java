package com.example.beautysalon.entity;

import com.example.beautysalon.converter.MasterTypeConverter;
import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@Setter
@Getter
@Table(name = "MASTERS")
@Entity
public class Master extends Person {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "QUALIFICATION")
    private String qualification;

    @Column(name = "MASTER_TYPE", nullable = false, updatable = false)
    @Convert(converter = MasterTypeConverter.class)
    private MasterType masterType;

    @Embedded
    private ContactDetails contactDetails;

    @ManyToMany(cascade = {CascadeType.DETACH, CascadeType.REFRESH})
    @JoinTable(name = "MASTER_OPERATION",
            joinColumns = @JoinColumn(name = "MASTER_ID"),
            inverseJoinColumns = @JoinColumn(name = "OPERATION_ID")
    )
    private Set<Operation> operations;

    @OneToMany(mappedBy = "master", cascade = CascadeType.ALL)
    private List<OrderItem> orderItems;
}
