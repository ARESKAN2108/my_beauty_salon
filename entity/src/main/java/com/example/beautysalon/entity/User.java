package com.example.beautysalon.entity;

import com.example.beautysalon.converter.UserRoleConverter;
import lombok.*;

import javax.persistence.*;
import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Setter
@Getter
@Table(name = "USERS")
@Entity
public class User extends BaseEntity<Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "LOGIN")
    private String login;

    @Column(name = "PASSWORD")
    private String password;

    @Convert(converter = UserRoleConverter.class)
    @Column(name = "USER_ROLE")
    private UserRole userRole;

    @ManyToMany(cascade = {CascadeType.DETACH, CascadeType.REFRESH})
    @JoinTable(name = "USER_PERM",
            joinColumns = @JoinColumn(name = "USER_ID"),
            inverseJoinColumns = @JoinColumn(name = "PERM_ID")
    )
    private Set<Permission> permissions;

    @Embedded
    private ContactDetails contactDetails;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name="street",column = @Column(name="street2")),
            @AttributeOverride(name="building",column = @Column(name="building2")),
            @AttributeOverride(name="city",column = @Column(name="city2")),
            @AttributeOverride(name="phone",column = @Column(name="phone2"))
    })
    private ContactDetails additional;
}
