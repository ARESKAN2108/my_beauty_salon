package com.example.beautysalon.entity;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Arrays;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Getter
public enum MasterType {

    NAIL_MASTER(Values.NAIL_MASTER),
    BROWS_MASTER(Values.BROW_MASTER),
    LASH_MAKER(Values.LASH_MAKER);

    private static final Map<String, MasterType> MAP = Arrays.stream(MasterType.values())
            .collect(Collectors.toMap(MasterType::getValue, Function.identity()));

    private final String value;

    public static MasterType getByValue(String value) {
        if (Objects.isNull(value)) {
            return null;
        }
        return MAP.get(value);
    }

    public static class Values {

        public static final String NAIL_MASTER = "NAIL_MASTER";
        public static final String BROW_MASTER = "BROW_MASTER";
        public static final String LASH_MAKER = "LASH_MAKER";
    }
}
