package com.example.beautysalon.entity;

import com.example.beautysalon.annotation.View;
import lombok.Getter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

@Getter
@Table(name = "client_order")
@View
@Entity
public class ClientOrder {

    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "ORDER_ID")
    private Long orderId;

    @Column(name = "TOTAL_AMOUNT")
    private BigDecimal totalAmount;

    @Column(name = "PAYER")
    private String payerName;

    @Column(name = "PAYER_PHONE")
    private String phone;
}
