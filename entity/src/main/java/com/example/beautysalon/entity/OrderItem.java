package com.example.beautysalon.entity;

import lombok.*;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Setter
@Getter
@Table(name = "ORDER_ITEMS")
@Entity
public class OrderItem extends BaseEntity<Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "OPERATION_ID")
    private Operation operation;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "MASTER_ID")
    private Master master;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "ORDER_ID")
    private Order order;
}
