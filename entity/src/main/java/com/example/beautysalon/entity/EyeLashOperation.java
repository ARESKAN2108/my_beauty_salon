package com.example.beautysalon.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@SuperBuilder
@DiscriminatorValue(value = OperationType.Values.EYELASHES)
@Entity
public class EyeLashOperation extends Operation {

    @Column(name = "EYELASH_OPERATION_NAME")
    private String eyelashOperationName;

}
