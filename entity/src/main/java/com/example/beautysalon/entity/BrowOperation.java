package com.example.beautysalon.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@SuperBuilder
@DiscriminatorValue(value = OperationType.Values.BROWS)
@Entity
public class BrowOperation extends Operation {

    @Column(name = "BROW_OPERATION_NAME")
    private String browOperationName;

}
