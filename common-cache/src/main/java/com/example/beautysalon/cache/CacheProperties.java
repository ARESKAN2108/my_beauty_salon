package com.example.beautysalon.cache;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Setter
@Getter
@Component
@ConfigurationProperties(prefix = "cache.ttl")
public class CacheProperties {

    private int masters;
    private int operations;
}
